package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class FreesurferQC extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FreesurferQC.class);
    public void finalProcessing(RunData data, Context context) {
        try{
            org.nrg.xdat.om.XnatMrsessiondata om = new org.nrg.xdat.om.XnatMrsessiondata(item);
            context.put("om",om);

        } catch(Exception e){
            logger.debug("FreeSurferQC ", e);
        }
    }

}
